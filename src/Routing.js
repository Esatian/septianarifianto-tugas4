import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import SplashScreen from './Screen/SplashScreen';
import AuntNavigation from './AuntNavigation';

const stack = createNativeStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <stack.Navigator screenOptions={{headerShown: false}}>
        <stack.Screen name="SplashScreen" component={SplashScreen} />
        <stack.Screen name="AuntNavigation" component={AuntNavigation} />
      </stack.Navigator>
    </NavigationContainer>
  );
}
