import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Login from './Screen/Login';

const stack = createNativeStackNavigator();

export default function AuntNavigation() {
  return (
    <stack.Navigator screenOptions={{headerShown: false}}>
      <stack.Screen name="Login" component={Login} />
    </stack.Navigator>
  );
}
