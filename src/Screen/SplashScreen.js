import React, {useEffect} from 'react';
import {View, Image} from 'react-native';

const SplashScreen = ({navigation, Route}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('AuntNavigation');
    }, 1000);
  }, []);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
      }}>
      <Image
        source={require('../Assets/Images/JackandFixer.png')}
        style={{width: 230, height: 238}}
      />
    </View>
  );
};

export default SplashScreen;
